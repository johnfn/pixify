/// <reference path="jquery.d.ts" />
/// <reference path="bootstrap.d.ts" />

var CANVAS_WIDTH:number = 500;
var CANVAS_HEIGHT:number = 500;

// Add a callback option for popovers.
var tmp = $.fn.popover.Constructor.prototype.show;

function arrEq(a1:number[], a2:number[]) {
	for (var i:number = 0; i < a1.length; i++) {
		if (a1[i] != a2[i]) return false;
	}

	return true;
}

$.fn.popover.Constructor.prototype.show = function () {
  tmp.call(this);
  if (this.options.callback) {
    this.options.callback();
  }
}

class RGB {
	r: number;
	g: number;
	b: number;

	constructor(r: number, g: number, b: number) {
		this.r = r;
		this.g = g;
		this.b = b;
	}

	static hexToRGB(hex):number[] {
	    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	    return result ? [
	        parseInt(result[1], 16),
	        parseInt(result[2], 16),
	        parseInt(result[3], 16)
	    ] : null;
    }

    static rgbaStrToRGB(str:string):number[] {
    	var result = /^rgba\(([0-9]{1,3}), ([0-9]{1,3}), ([0-9]{1,3})/.exec(str);

		return result ? [ parseInt(result[1]), parseInt(result[2]), parseInt(result[3])] : null;
    }

    static componentToHex(c:number) {
	    var hex = c.toString(16);
	    return hex.length == 1 ? "0" + hex : hex;
    }

	static rgbToHex(r:number, g:number, b:number) {
	    return "#" + RGB.componentToHex(r) + RGB.componentToHex(g) + RGB.componentToHex(b);
	}
}

class Point {
	x: number;
	y: number;

	constructor(x: number, y: number) {
		this.x = x;
		this.y = y;
	}

	set(x: number, y: number) {
		this.x = x;
		this.y = y;
	}

	copy():Point {
		return new Point(this.x, this.y);
	}

	static stringToPoint(str:string):Point {
		var xy:string[] = str.split(",");

		return new Point(parseInt(xy[0]), parseInt(xy[1]));
	}

	toString() {
		return this.x + "," + this.y;
	}
}

// Currently:
//
// * Normalizes x and y positions to relative to the target element rather
//   than absolute
// * Keeps track of whether the mouse is pressed or not.
// * Keeps track of where a drag started and ended. May be the same place if
//   it's just a click.

class MouseHandler {
	private movecb:(m:MouseInfo) => void;
	private downcb:(m:MouseInfo) => void;
	private upcb:(m:MouseInfo) => void;

	x:number = 0;
	y:number = 0;
	isDown:boolean = false;
	dragStartPosition:Point = new Point(0, 0);
	dragEndPosition:Point = new Point(0, 0);
	$target:JQuery;

	insideElement:boolean = false;
	downOnElement:boolean = false; // true if the user is currently pressing the mouse, and started the click on the element. 

	constructor(elem:JQuery , movecb:(m:MouseInfo) => void
							, downcb:(m:MouseInfo) => void
							, upcb:(m:MouseInfo) => void) {

		elem.on("mousemove",  $.proxy(this.mouseMove, this));
		elem.on("mousedown",  $.proxy(this.mouseDown, this));
		elem.on("mouseup",    $.proxy(this.mouseUp, this));
		elem.on("mouseenter", $.proxy(this.mouseEnter, this));
		elem.on("mouseleave", $.proxy(this.mouseLeave, this));

		$(document).on("mousemove", $.proxy(this.mouseMoveOutside, this));
		$(document).on("mouseup",   $.proxy(this.mouseUpOutside, this));

		this.movecb = movecb;
		this.downcb = downcb;
		this.upcb   = upcb;
		this.$target = elem;
	}

	private mouseEnter(e:JQueryEventObject):void {
		this.insideElement = true;
	}

	private mouseLeave(e:JQueryEventObject):void {
		this.insideElement = false;
	}

	private normalize(e:JQueryEventObject):void {
		var parentOffset = this.$target.offset();

		// Normalize x/y
		this.x = e.pageX - parentOffset.left;
		this.y = e.pageY - parentOffset.top;
	}

	private info():MouseInfo {
		return new MouseInfo(this.x, this.y, this.isDown, this.dragStartPosition.copy(), this.dragEndPosition.copy(), this.$target);
	}

	private mouseMove(e:JQueryEventObject) {
		this.normalize(e);

		if (this.isDown) {
			this.dragEndPosition.set(this.x, this.y);
		}

		if (this.movecb) this.movecb(this.info());
	}

	private mouseMoveOutside(e:JQueryEventObject) {
		if (this.downOnElement && !this.insideElement) {
			this.mouseMove(e);
		}
	}

	private mouseUpOutside(e:JQueryEventObject) {
		this.normalize(e);

		this.isDown        = false;
		this.downOnElement = false;
	}

	private mouseDown(e:JQueryEventObject) {
		this.normalize(e);
		
		this.isDown = true;
		this.downOnElement = this.insideElement;
		this.dragStartPosition.set(this.x, this.y);
		
		if (this.downcb) this.downcb(this.info());
	}

	private mouseUp(e:JQueryEventObject) {
		this.normalize(e);
		
		this.isDown = false;
		this.downOnElement = false;
		this.dragEndPosition.set(this.x, this.y);

		if (this.upcb) this.upcb(this.info());
	}
}

class MouseInfo {
	x:number = 0;
	y:number = 0;
	isDown:boolean = false;
	dragStartPosition:Point = new Point(0, 0);
	dragEndPosition:Point = new Point(0, 0);
	target:JQuery;

	constructor(x:number, y:number, isDown:boolean, dragStartPosition:Point, dragEndPosition:Point, target:JQuery) {
		this.x = x;
		this.y = y;
		this.isDown = isDown;
		this.dragStartPosition = dragStartPosition;
		this.dragEndPosition = dragEndPosition;
		this.target = target;
	}
}

class Keyboard {
	downCallback: (key:string) => void = undefined;
	upCallback: (key:string) => void = undefined;

	static shiftKey:boolean = false;

	constructor() {
		$(document).on("keydown", $.proxy(this.keyDown, this));
		$(document).on("keyup", $.proxy(this.keyUp, this));
	}

	getKey(e:JQueryEventObject):string {
		var key = String.fromCharCode(e.which).toLowerCase();
		if (e.shiftKey) {
			key = key.toUpperCase();
		}

		return key;
	}

	keyDown(e:JQueryEventObject) {
		if (this.downCallback != undefined) {
			this.downCallback(this.getKey(e));
		}

		Keyboard.shiftKey = e.shiftKey;
	}

	keyUp(e:JQueryEventObject) {
		if (this.upCallback != undefined) {
			this.upCallback(this.getKey(e));
		}

		Keyboard.shiftKey = e.shiftKey;
	}

	addKeydownListener(callback: (key:string) => void):Keyboard {
		this.downCallback = callback;

		return this;
	}

	addKeyupListener(callback: (key:string) => void):Keyboard {
		this.upCallback = callback;

		return this;
	}
}

class Preview {
	private preview:HTMLCanvasElement;
	private previewContext:CanvasRenderingContext2D;
	private mouse:MouseHandler;
	private canvas:Canvas;
	dirty:boolean = false;

	static previewWindow:Preview;

	zoomWindowX:number = 0;
	zoomWindowY:number = 0;
	zoomWindowWidth:number = 0;
	zoomWindowHeight:number = 0;

	constructor(canvas:Canvas) {
		if (Preview.previewWindow) {
			console.error("There should only be one Preview window.");
		}

		Preview.previewWindow = this;

		this.preview = <HTMLCanvasElement> document.getElementById('preview');

		this.previewContext = this.preview.getContext('2d');

		var update = $.proxy(this.updateMouse, this);

		this.mouse = new MouseHandler($(this.preview), update, update, update);
		this.canvas = canvas;
	}

	updateMouse(mouse:MouseInfo) {
		if (mouse.isDown) {
			this.moveZoomWindow(mouse.x - this.zoomWindowWidth / 2, mouse.y - this.zoomWindowHeight / 2);
		}
	}

	moveZoomWindow(x:number, y:number):void {
		this.zoomWindowX = x;
		this.zoomWindowY = y;

		if (this.zoomWindowX < 0) this.zoomWindowX = 0;
		if (this.zoomWindowY < 0) this.zoomWindowY = 0;

		if (this.zoomWindowX + this.zoomWindowWidth  > this.preview.width)  this.zoomWindowX = this.preview.width  - this.zoomWindowWidth;
		if (this.zoomWindowY + this.zoomWindowHeight > this.preview.height) this.zoomWindowY = this.preview.height - this.zoomWindowHeight;

		this.dirty = true;
	}

	update() {
		var img:Graphic = this.canvas.renderImage();
		this.previewContext.fillStyle = "white";
		this.previewContext.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

		this.previewContext.drawImage(img.content, 0, 0);

		img.remove();

		// draw zoom window
		this.zoomWindowWidth = this.canvas.width / this.canvas.scale;
		this.zoomWindowHeight = this.canvas.height / this.canvas.scale;

		this.previewContext.strokeRect(this.zoomWindowX, this.zoomWindowY, this.zoomWindowWidth, this.zoomWindowHeight);
	}
}

class UndoStep {
	data:Graphic;
	toolName:string;
	$elem:JQuery;

	constructor(data:Graphic, toolName:string) {
		this.data = data;
		this.toolName = toolName;
	}
}

class Layer {
	name:string;
	graphic:Graphic;

	$listItem:JQuery;
	$listDiv:JQuery;
	$listText:JQuery;

	constructor(name:string) {
		this.name = name;
		this.graphic = new Graphic();
	}
}

class LayerList {
	list:Layer[] = [];
	$list:JQuery;
	selectedLayer:number = 0;
	canvas:Canvas;

	constructor(canvas:Canvas) {
		var $layerDiv = $('#layers');
		var $newLayer = $('<a/>', { html: "Add layer", href: "#" });
		this.$list = $('<ol/>');
		this.canvas = canvas;

		$layerDiv.append(
			$('<div/>', { html: 'Layers:'
						}).css('font-weight', 'thick')
		).append(this.$list).append($newLayer);

		$newLayer.click(() => {
			this.addLayer(true);
		});

		this.addLayer(false);

		this.changeSelectedLayer(0);
	}

	getSelectedLayer() {
		return this.list[this.selectedLayer];
	}

	changeSelectedLayer(which:number) {
		var oldLayer:Layer = this.list[this.selectedLayer];
		var newLayer:Layer = this.list[which];

		this.selectedLayer = which;
		oldLayer.$listDiv.removeClass("selected");
		newLayer.$listDiv.addClass("selected");
	}

	addLayer(inputName:boolean) {
		var layerName:string = "Layer " + this.list.length;
		var layerNumber:number = this.list.length;
		var l:Layer = new Layer(layerName);
		this.list.push(l);

		this.addLayerToDocument(layerName, inputName, l, layerNumber);

		this.canvas.addLayer(l);
	}

	private makeLayerToolbar(layer:Layer, layerDiv:JQuery):JQuery {
		var result:JQuery = $('<span/>');

		var $remove:JQuery = $("<a/>", {href: "#", html: "[X]"}).mousedown(() => {
			this.list.splice(this.list.indexOf(layer), 1);
			layerDiv.remove();
			layer.graphic.remove();

			this.canvas.removeLayer(layer);
		});

		var $hideThis:JQuery = $("<input/>", {type: "checkbox"}).mousedown(() => {
			layer.graphic.toggleVisible();
			this.canvas.render();
		});

		var $hideOthers:JQuery = $("<a/>", {href: "#", html: "hide others"}).mousedown(() => {
			console.log("hide others pressed");
		});

		result.append($remove).append(" ")
			  .append($hideThis).append(" ")
			  .append($hideOthers);

		return result;
	}

	private addLayerToDocument(layerName:string, inputName:boolean, newLayer:Layer, layerNumber:number) {
		var $listItem = $('<li/>');
		var $listDiv = $('<div/>', { html: layerName }).append(" ");
		var $layerTools = this.makeLayerToolbar(newLayer, $listItem);

		$listDiv.append($layerTools).dblclick(() => {
			$listDiv.replaceWith($listText);
			$listText.focus();
			$listText.select();
		}).click(() => {
			this.changeSelectedLayer(layerNumber);
		});

		var $listText:JQuery = $('<input/>', { type: "text" }).val(layerName).keypress((e:JQueryEventObject) => {
			if (e.which == 13) { // Enter
				$listDiv.html($listText.val());
				$listText.replaceWith($listDiv);
				layerName = $listText.val();
			}
		});

		$listItem.append($listDiv);
		this.$list.append($listItem);

		if (inputName) {
			$listItem.trigger("dblclick");
			$listText.focus();
		}

		newLayer.$listDiv = $listDiv;
		newLayer.$listText = $listText;
		newLayer.$listItem = $listItem;
	}

	removeLayer() {
		console.log("TODO");
	}
}

class UndoHistory {
	stack:UndoStep[] = [];
	canvas:Canvas;
	currentStep:number = 0;

	constructor(canvas:Canvas) {
		this.canvas = canvas;
	}

	addStep(data:Graphic, toolName:string) {
		var step:number = this.stack.length;
		var that:UndoHistory = this;
		var undoStep:UndoStep = new UndoStep(data, toolName);
		var $elem:JQuery;

		var popoverContent = () => {
			var canvas:HTMLCanvasElement = document.createElement('canvas');
			canvas.width = CANVAS_WIDTH;
			canvas.height = CANVAS_HEIGHT;
			canvas.getContext('2d').putImageData(this.stack[step].data.context.getImageData(0,0,500,500), 0, 0);

			return canvas;
		};

		$elem = $('<li/>', {
					        html: $('<a/>', 
							        { href: '#'
						            , text: toolName
						            , title: "Preview"
							        }).popover({ trigger: "hover" 
							                   , html: true
							                   , content: popoverContent
							                   })
				            , 'data-content': "tooltip",

						    }).click((e:JQueryEventObject) => {
						    	that.revertTo(step);
			    			}).appendTo("#undo-list");

		undoStep.$elem = $elem;

	 	if (this.currentStep != this.stack.length) {
	 		// we're about to obliterate the undo history.

	 		for (var i:number = this.currentStep + 1; i < this.stack.length; i++) {
	 			this.stack[i].$elem.remove();
	 		}

	 		this.currentStep = this.stack.length;
	 	}

		++this.currentStep;
		this.stack.push(undoStep);
	}

	revertTo(whichStep:number) {
		Graphic.undo(whichStep, this.canvas.displayList);

		// visually indicate that later undo steps are disabled

		for (var i:number = 0; i < this.stack.length; i++) {
			var newColor:string;
			if (i <= whichStep) {
				newColor = "blue";
			} else {
				newColor = "lightgray";
			}

			this.stack[i].$elem.children("a").css("color", newColor);
		}

		this.currentStep = whichStep;
	}
}

// This represents anything you can draw on.
class Graphic {
	private masterContent:HTMLCanvasElement;
	private masterContext:CanvasRenderingContext2D;
	private dirty:boolean = false;

	static displayList:Graphic[] = [];
	static hasInitialized:boolean = false;

	undoStack:Graphic[] = [];

	content:HTMLCanvasElement;
	context:CanvasRenderingContext2D;
	imgData:Uint8Array;
	visible:boolean = true;

	width:number;
	height:number;
	x:number = 0;
	y:number = 0;
	cachedImageData:ImageData;
	cachedImageDataData:Uint8Array; // Bad name, but that's what you get when you have a data field on something named imagedata.

	locked:boolean = false;
	fixed:boolean = false;
	changed:boolean = false; // for undo.

	constructor(canvas:HTMLCanvasElement = null) {
		if (canvas == null) {
			this.content = <HTMLCanvasElement> $("<canvas>").attr("width", CANVAS_WIDTH).attr("height", CANVAS_HEIGHT)[0];
			this.context = this.content.getContext("2d");
		} else {
			this.content = canvas;
			this.context = this.content.getContext("2d");
		}

		this.masterContent = <HTMLCanvasElement> $("<canvas>").attr("width", CANVAS_WIDTH).attr("height", CANVAS_HEIGHT)[0];
		this.masterContext = this.masterContent.getContext("2d");

		this.context.globalAlpha = 1;

		this.context.fillStyle = "rgba(255,255,255,0)";
		this.context.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

		(<any>this.context).webkitImageSmoothingEnabled = false;
	    (<any>this.context).mozImageSmoothingEnabled = false;

	    this.width  = CANVAS_WIDTH;
	    this.height = CANVAS_HEIGHT;

	    Graphic.displayList.push(this);

	    if (!Graphic.hasInitialized) {
	    	Graphic.hasInitialized = true;

			var updateGfx = (function() {
				var dc:number = 0;

				for (var i:number = 0; i < Graphic.displayList.length; i++) {
					var graphic:Graphic = Graphic.displayList[i];

					if (!graphic.dirty && !Preview.previewWindow.dirty) continue;

					++dc;

					if (graphic.fixed) {
						graphic.updateGraphic(graphic.x, graphic.y);
					} else {
						graphic.updateGraphic(-Preview.previewWindow.zoomWindowX + graphic.x, -Preview.previewWindow.zoomWindowY + graphic.y);
					}
				}

				 $("#dirty-count").text("Dirty: " + dc);
				 $("#canvas-count").text("Canvases: " + Graphic.displayList.length);

				Preview.previewWindow.update();
				Preview.previewWindow.dirty = false;

				window.requestAnimationFrame(updateGfx);
			});

			window.requestAnimationFrame(updateGfx);
		};

		this.updateGraphic();
	}

	static addUndoStep(list:Graphic[]) {
		for (var i:number = 0; i < list.length; i++) {
			var graphic:Graphic = list[i];

			if (graphic.changed) {
				graphic.undoStack.push(graphic.clone());

				graphic.changed = false;
			} else {
				graphic.undoStack.push(graphic.undoStack[list.length - 1]); // unchanged, so use same graphic.
			}
		}
	}

	static undo(steps:number, list:Graphic[]) {
		for (var i:number = 0; i < list.length; i++) {
			var graphic:Graphic = list[i];

			graphic.clear();
			graphic.undoStack[steps].renderTo(graphic, 0, 0);
		}
	}

	remove() {
		var index:number = Graphic.displayList.indexOf(this);
		Graphic.displayList.splice(index, 1);
	}

	// needs to be called from requestAnimationFrame callback.
	updateGraphic(offsetX:number = 0, offsetY:number = 0) {
		if (this.locked) {
			console.error("called updateGraphic while locked");
			return;
		}

		this.context.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

		if (this.visible) {
			this.context.drawImage(this.masterContent, offsetX, offsetY);
		}

		this.cachedImageData = this.masterContext.getImageData(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
		this.cachedImageDataData = this.cachedImageData.data;
		this.dirty = false;
		this.changed = true;
	}

	clear() {
		this.dirty = true;

		this.masterContext.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
	}

	clearRect(x:number, y:number, w:number, h:number) {
		this.dirty = true;
		
		this.masterContext.clearRect(x, y, w, h);
	}

	// call before doing a lot of intensive graphics work. will optimize all setPixel() calls.
	lock() {
		this.locked = true;
	}

	// call when you've finished doing your work.
	unlock() {
		this.locked = false;

		this.masterContext.putImageData(this.cachedImageData, 0, 0);
	}

	// Note that this is different from html5 canvas scaling in that it retroactively scales everything you've already drawn.
	// That seems like expected behavior anyways... 
	scale(x:number, y:number) {
		this.dirty = true;

		// reset scale to (1, 1)
		this.context.setTransform(1,0,0,1,0,0);
		this.context.scale(x, y);
	}

	renderTo(dest:Graphic, offsetX:number, offsetY:number) {
		dest.dirty = true;

		dest.masterContext.drawImage(this.content, offsetX, offsetY);
	}

	clone():Graphic {
		var result:Graphic = new Graphic();
		this.renderTo(result,0,0);
		result.updateGraphic(0,0);

		return result;
	}

	renderToUnscaled(dest:Graphic, offsetX:number, offsetY:number) {
		dest.dirty = true;

		dest.masterContext.drawImage(this.masterContent, offsetX, offsetY);
	}

	setVisible(v:boolean) {
		this.dirty = true;

		this.visible = v;
	}

	toggleVisible() {
		this.dirty = true;

		this.visible = !this.visible;
	}

	isVisible():boolean {
		this.dirty = true;

		return this.visible;
	}

	contains(p:Point) {
		return p.x >= 0 && p.x < CANVAS_WIDTH && p.y >= 0 && p.y < CANVAS_HEIGHT;
	}

	setPixel(x:number, y:number, color: number[]) {
		this.dirty = true;

		if (!this.locked) {
			this.masterContext.fillStyle = RGB.rgbToHex(color[0],color[1],color[2]);
			this.masterContext.fillRect(x, y, 1, 1);
		} else {
			var offset = 2000 * y + x * 4;

			this.cachedImageDataData[offset + 0] = 255;
			this.cachedImageDataData[offset + 1] = 0;
			this.cachedImageDataData[offset + 2] = 255;
			this.cachedImageDataData[offset + 3] = 1;
		}
	}

	/*
	private setPixelHaxor(x:number, y:number) {
		var offset = this.content.height * 4 * y + x * 4;

		if (!this.imgData) {
			this.imgData = this.context.getImageData(0,0,this.content.width,this.content.height).data;
		}

		this.imgData[0 + offset] = 0;
		this.imgData[1 + offset] = 0;
		this.imgData[2 + offset] = 0;
		this.imgData[3 + offset] = 0;
	}
	*/

	private xyToIDOffset(x:number, y:number) {
		return this.content.width * 4 * y + x * 4;
	}

	getPixel(x:number, y:number):number[] {
		var offset = this.content.width * 4 * y + x * 4;
        var data = [this.cachedImageDataData[0 + offset], 
					this.cachedImageDataData[1 + offset], 
					this.cachedImageDataData[2 + offset], 
					this.cachedImageDataData[3 + offset]];
        return data;
	}

	fillRect(x:number, y:number, width:number, height:number, color: number[]) {
		this.dirty = true;

		this.masterContext.fillStyle = RGB.rgbToHex(color[0], color[1], color[2]);
		this.masterContext.fillRect(x, y, width, height);
	}

	strokeRect(x:number, y:number, width:number, height:number, color: number[]) {
		this.dirty = true;

		this.masterContext.fillStyle = RGB.rgbToHex(color[0], color[1], color[2]);
		this.masterContext.strokeRect(x, y, width, height);
	}
}

// Generic tool class. Takes a setPixel callback function that Canvas
// implements so it doesn't have to get tied up in graphic logic.
// TODO: Don't like how you have to pass in translate currently. 
class Tool {
	constructor(public layers:LayerList, public firstColor:() => number[], public secondColor:() => number[], public canvas:Canvas) {

	}

	// There is no protected keyword O_O
	mouseUp(mouse:MouseInfo) {
		if (this.addsUndoStep()) {
			this.canvas.addUndoStep(this.toolName()); // seems like a bug in TypeScript?
		}
	}
	mouseDown(mouse:MouseInfo) {}
	mouseMove(mouse:MouseInfo) {}
	toolName():string { return "Oops!"; }

	getGraphic():Graphic {
		return this.layers.getSelectedLayer().graphic;
	}

	// some handy methods so you don't have to constantly call getGraphic().
	getPixel(x:number, y:number):number[] { return this.getGraphic().getPixel(x, y); }
	setPixel(x:number, y:number, color:number[]) { this.getGraphic().setPixel(x, y, color); }

	showsGhost():boolean {
		return true;
	}

	addsUndoStep():boolean {
		return true;
	}
}

class Brush extends Tool {
	oldPoint:Point = null;

	currentGraphic:Graphic = null;
	currentStraightGraphic:Graphic = null;

	getGraphic():Graphic {
		if (Keyboard.shiftKey) {
			return this.currentStraightGraphic;
		} else {
			return this.currentGraphic;
		}
	}

	mouseUp(mouse:MouseInfo) {
		var currentPoint:Point = new Point(mouse.x, mouse.y);

		this.drawLine(this.oldPoint.x, this.oldPoint.y, currentPoint.x, currentPoint.y, this.firstColor()); 
		this.oldPoint = null;

		var target:Graphic = this.layers.getSelectedLayer().graphic;

		if (Keyboard.shiftKey) {
			this.currentStraightGraphic.renderToUnscaled(target, 0, 0);
		} else {
			this.currentGraphic.renderToUnscaled(target, 0, 0);
		}

		this.canvas.removeGraphic(this.currentGraphic);
		this.canvas.removeGraphic(this.currentStraightGraphic);

		super.mouseUp(mouse);
	}

	// Draw a line with no anti-aliasing.
	drawLine(startX:number, startY:number, endX:number, endY:number, color: number[]) {
		var dx:number = endX - startX;
		var dy:number = endY - startY;

		var mag:number = Math.sqrt(dx * dx + dy * dy);

		dx /= mag;
		dy /= mag;

		var xSteps:number = Math.ceil((endX - startX) / dx);
		var ySteps:number = Math.ceil((endY - startY) / dy);

		if (isNaN(xSteps)) xSteps = 0;
		if (isNaN(ySteps)) ySteps = 0;

		var steps:number = Math.max(xSteps, ySteps, 1); // Need to take at least one step always - if they just click on a single pixel.
		var currentX:number = startX;
		var currentY:number = startY;

		for (var i = 0; i < steps; i++) {
			var x:number = Math.floor(currentX);
			var y:number = Math.floor(currentY);

			this.setPixel(x, y, color);

			currentX += dx;
			currentY += dy;
		}
	}

	setPixel(x:number, y:number, color:number[]):void {
		var size:number = this.getSize();
		var halfsize:number = Math.floor(this.getSize() / 2);
		var halfsize2:number = Math.ceil(this.getSize() / 2);

		for (var i:number = x - halfsize; i < x + halfsize2; i++) {
			for (var j:number = y - halfsize; j < y + halfsize2; j++) {
				super.setPixel(i, j, color);
			}
		}

		ColorPalette.colorPalette.addColor(color);
	}

	getSize():number {
		return parseInt($("#brush-size").val());
	}

	mouseDown(mouse:MouseInfo) {
		this.currentGraphic = new Graphic();
		this.currentStraightGraphic = new Graphic();

		this.currentGraphic.scale(this.canvas.scale, this.canvas.scale);
		this.currentStraightGraphic.scale(this.canvas.scale, this.canvas.scale);

		this.canvas.addGraphic(this.currentGraphic);
		this.canvas.addGraphic(this.currentStraightGraphic);

		this.oldPoint = new Point(mouse.x, mouse.y);
	}

	mouseMove(mouse:MouseInfo) {
		if (mouse.isDown) {
			var currentPoint:Point = new Point(mouse.x, mouse.y);

			if (Keyboard.shiftKey) {
				this.currentStraightGraphic.visible = true;
				this.currentGraphic.visible = false;
			} else {
				this.currentStraightGraphic.visible = false;
				this.currentGraphic.visible = true;
			}

			if (this.currentGraphic.visible) {
				if (this.oldPoint != null) {
					this.drawLine(this.oldPoint.x, this.oldPoint.y, currentPoint.x, currentPoint.y, this.firstColor()); 
				}
			} else if (this.currentStraightGraphic.visible) {
				this.currentStraightGraphic.clear();

				if (Math.abs(mouse.dragStartPosition.x - mouse.dragEndPosition.x) > Math.abs(mouse.dragStartPosition.y - mouse.dragEndPosition.y)) {
					this.drawLine( mouse.dragStartPosition.x, mouse.dragStartPosition.y
							 								, mouse.dragEndPosition.x,   mouse.dragStartPosition.y, this.firstColor());
				} else {
					this.drawLine( mouse.dragStartPosition.x, mouse.dragStartPosition.y
							                                , mouse.dragStartPosition.x, mouse.dragEndPosition.y, this.firstColor());
				}
			}

			this.oldPoint = currentPoint;
		}
	}

	toolName():string {
		return "Brush";
	}
}

class Eyedropper extends Tool {
	canvas:Canvas;

	constructor(layers:LayerList, firstColor:() => number[], secondColor:() => number[], public setFirstColor:(s:number[]) => void, canvas:Canvas) {
		super(layers, firstColor, secondColor, canvas);

		this.canvas = canvas;
	}

	showsGhost():boolean {
		return false;
	}

	mouseDown(mouse:MouseInfo) {
		var g:Graphic = this.canvas.renderImage();

		this.setFirstColor(g.getPixel(mouse.x, mouse.y));

		g.remove();
	}

	toolName():string {
		return "Eyedropper";
	}

	addsUndoStep():boolean {
		return false;
	}
}

class PaintBucket extends Tool {
	toolName():string {
		return "PaintBucket";
	}

	mouseDown(mouse:MouseInfo) {
		var p:Point = new Point(mouse.x, mouse.y);
		var color:number[] = this.firstColor();
		var tried: {[key: string]: boolean; } = {};
		tried[p.toString()] = true;

		var justTried:Point[] = [p];
		var oldColor:number[] = this.getPixel(p.x, p.y); // color of the section we are filling.

		this.setPixel(p.x, p.y, color);

		var neighborDX = [0, 0, -1, 1];
		var neighborDY = [1, -1, 0, 0];

		while (justTried.length != 0) {
			var outerEdgeOfFill:Point[] = [];

			for (var i:number = 0; i < justTried.length; i++) {
				var currentPoint:Point = justTried[i];

				for (var j:number = 0; j < neighborDY.length; j++) {
					var dx = neighborDX[j];
					var dy = neighborDY[j];
					var n:Point = new Point(currentPoint.x + dx, currentPoint.y + dy);

					if (!this.getGraphic().contains(n)) continue;
					if (!arrEq(this.getPixel(n.x, n.y), oldColor)) continue;
					if (tried[n.toString()]) continue;

					outerEdgeOfFill.push(n);
					tried[n.toString()] = true;
					this.setPixel(n.x, n.y, color);
				}
			}

			justTried = outerEdgeOfFill;
		}
	}
}

class Eraser extends Brush {
	constructor(layers:LayerList, firstColor:() => number[], secondColor:() => number[], canvas:Canvas) {
		super(layers, secondColor, secondColor, canvas);
	}

	toolName():string {
		return "Eraser";
	}
}

//TODO probably should separate input from gfx.
// Probably shouldn't do new Preview(), from a philosophical point of view.
class Canvas {
	width:number = CANVAS_WIDTH;
	height:number = CANVAS_HEIGHT;
	mouse:MouseHandler;
	keyboard:Keyboard;

	scale:number = 1.0;

	colorPicker:ColorPicker;

	ghost:Graphic;

	preview:Preview;

	tools:Tools;
	layers:LayerList;
	undoHistory:UndoHistory;

	renderedImage:Graphic;

	displayList:Graphic[] = [];

	transparentBG:Graphic;
	canvasBoundary:Graphic;

	constructor() {
		this.undoHistory = new UndoHistory(this);
		this.canvasBoundary = new Graphic(<HTMLCanvasElement> $("#topcanvas")[0])
		this.makeTransparentBackground();
		this.mouse = new MouseHandler($("#canvas-stack"), $.proxy(this.mouseMove, this), $.proxy(this.mouseDown, this), $.proxy(this.mouseUp, this));
		this.keyboard = new Keyboard().addKeydownListener($.proxy(this.keyDown, this));
		this.layers = new LayerList(this);
		this.tools = new Tools(this.layers, $.proxy(this.translatePoint, this), this);
		this.preview = new Preview(this);
		this.addUndoStep("New Document");

		this.ghost = new Graphic();
		this.ghost.setPixel(0, 0, [0, 0, 0, 1]);
		this.addGraphic(this.ghost);
		new ColorPalette(this, this.tools);
	}

	addLayer(l:Layer) {
		l.graphic.scale(this.scale, this.scale);

		this.addGraphic(l.graphic);
	}

	removeLayer(l:Layer) {
		this.removeGraphic(l.graphic);
		l.graphic.remove();
	}

	makeTransparentBackground() {
		var blockSize:number = 15;

		this.transparentBG = new Graphic(<HTMLCanvasElement> $("#bg")[0]);
		this.transparentBG.fixed = true;

		for (var i:number = 0; i < this.width; i += blockSize) {
			for (var j:number = 0; j < this.width; j += blockSize) {
				if (((i / blockSize) + (j / blockSize)) % 2 == 0) {
					this.transparentBG.fillRect(i, j, blockSize, blockSize, [200, 200, 200, 1]);
				}
			}
		}
	}

	makeCanvasBoundary() {
		var effectiveWidth:number  = this.width * this.scale;
		var effectiveHeight:number = this.height * this.scale;

		this.canvasBoundary.fillRect(0, 0, this.canvasBoundary.width, this.canvasBoundary.height, [80, 80, 80, 1]);
		this.canvasBoundary.clearRect(0, 0, effectiveWidth, effectiveHeight);
		this.canvasBoundary.strokeRect(0, 0, effectiveWidth, effectiveHeight, [0, 0, 0, 1]);
	}

	addGraphic(g:Graphic) {
		this.displayList.push(g);

		var $canvas:JQuery = $(g.content);
		$canvas.css({ "position": "absolute", "z-index" : "" + this.displayList.length}).attr("id", "layer" + this.displayList.length );
		$("#canvas-stack").append($canvas);
	}

	removeGraphic(g:Graphic) {
		var index = this.displayList.indexOf(g);

		if (index == -1) {
			console.error("Couldn't find graphic to remove.");
			return;
		}

		this.displayList.splice(index, 1);
		$(g.content).remove();
	}

	render() { }

	zoom(delta:number) {
		this.scale += delta;

		for (var i:number = 0; i < this.displayList.length; i++) {
			this.displayList[i].scale(this.scale, this.scale);
		}
	}

	keyDown(key:string) {
		if (key == "z") {
			this.zoom(0.1);
		}

		if (key == "Z") {
			this.zoom(-0.1);
		}

		if (key == "1") {
			$("#debugging").toggle();
		}

		this.makeCanvasBoundary();
	}


	renderImage():Graphic {
		var renderImageGraphic:Graphic = new Graphic();

		for (var i:number = 0; i < this.displayList.length; i++) {
			this.displayList[i].updateGraphic(0, 0);
			this.displayList[i].renderToUnscaled(renderImageGraphic, 0, 0);
		}

		renderImageGraphic.updateGraphic();

		return renderImageGraphic;
	}

	addUndoStep(toolName:string) {
		if (this.tools.currentTool.addsUndoStep()) {
			this.undoHistory.addStep(this.renderImage(), toolName);

			Graphic.addUndoStep(this.displayList);
		}
	}

	translatePoint(pt:Point):Point {
		pt.x /= this.scale;
		pt.y /= this.scale;

		pt.x += this.preview.zoomWindowX;
		pt.y += this.preview.zoomWindowY;

		pt.x = Math.floor(pt.x);
		pt.y = Math.floor(pt.y);

		return pt;
	}

	mouseMove(mouse:MouseInfo) {
		var pt:Point = new Point(mouse.x, mouse.y);
		pt = this.translatePoint(pt);

		this.ghost.x = pt.x;
		this.ghost.y = pt.y;
	}
}

class ColorSwatch {
	$canvas:JQuery;
	ctx:CanvasRenderingContext2D;
	color:number[] = [255, 0, 0];

	size:number = 10;

	constructor(color:number[] = [255, 0, 0]) {
		this.color = color;
		this.$canvas = $("<canvas/>", {"title": " lalalal"});
		(<HTMLCanvasElement> this.$canvas[0]).width  = this.size;
		(<HTMLCanvasElement> this.$canvas[0]).height = this.size;
		this.ctx = (<HTMLCanvasElement> this.$canvas[0]).getContext("2d");

		this.$canvas.tooltip();

		var that:ColorSwatch = this;

		this.$canvas.on("mouseover", function() {
			that.$canvas.css({"border-color": "#ccc"});
		}).on("mouseout", function() {
			that.$canvas.css({"border-color": "#000"});
		});

		this.updateSwatch();
	}

	onClick(f) {
		this.$canvas.on("click", f);
	}

	updateSwatch() {
		this.ctx.fillStyle = "rgba(" + this.color[0] + ", " + this.color[1] + ", " + this.color[2] + ", 1)";
		this.ctx.fillRect(0, 0, this.size, this.size);

		// update the tooltip
		this.$canvas.attr("data-original-title", "RGB " + this.color[0] + ", " + this.color[1] + ", " + this.color[2]);
	}

	setColor(color:number[]) {
		for (var i:number = 0 ; i < color.length; i++) {
			if (color[i] < 0)   color[i] = 0;
			if (color[i] > 255) color[i] = 255;
		}
		this.color = color;

		this.updateSwatch();
	}

	getCanvas():JQuery {
		return this.$canvas;
	}
}

class ColorPalette {
	static colorPalette:ColorPalette;
	private colors:{[key: string]: boolean; } = {};
	private colorCounts:{[key: string]: number; } = {"0 0 0": 500 * 500}
	private t:Tools;

	canvas:Canvas;

	constructor(c:Canvas, t:Tools) {
		ColorPalette.colorPalette = this;

		this.t = t;
		this.canvas = c;
		this.addColor([0, 0, 0]);
	}

	addColor(rgb:number[]) {
		var color:string = rgb.join(" ");
		var that:ColorPalette = this;
		var arrClone:number[] = [rgb[0], rgb[1], rgb[2], rgb[3]];

		if (!this.colors[color]) {
			this.colors[color] = true;
			var newSwatch:ColorSwatch = new ColorSwatch(rgb);
			$("#palette").append(newSwatch.getCanvas());

			newSwatch.onClick(function() {
				that.t.setFirstColor(arrClone);
			});
		}
	}
}

class ColorPicker {
	private colors:string[] = ["r", "g", "b"];
	private contexts:CanvasRenderingContext2D[] = [];
	private canvases:JQuery[] = [];

	private selectedColorContext:CanvasRenderingContext2D;

	private selectedColor:number[] = [50, 200, 50]; // using a class is really just a pain, since you need to index into this eventually
	private width:number = 255;
	private height:number = 10;

	private swatch:ColorSwatch;

	constructor() {
		var mouseUpdateProxy = $.proxy(this.mouseUpdate, this);

		for (var i:number = 0; i < this.colors.length; i++) {
			var $canvas = $("#picker-" + this.colors[i]);
			var canvas:HTMLCanvasElement = <HTMLCanvasElement> $canvas[0];

			canvas.width = this.width;
			canvas.height = this.height;

			new MouseHandler($canvas, mouseUpdateProxy, mouseUpdateProxy, mouseUpdateProxy);

			this.canvases.push($canvas);
			this.contexts.push(canvas.getContext("2d"));
		}

		this.swatch = new ColorSwatch();
		$("#picker-swatch").replaceWith(this.swatch.$canvas);

		this.updateSwatch();
		this.recolorSliders();
	}

	getSelectedColor():number[] {
		return this.selectedColor;
	}

	setSelectedColor(color:number[]):void {
		this.selectedColor = color;

		this.updateSwatch();
		this.recolorSliders();
	}

	private mouseUpdate(m:MouseInfo) {
		if (m.isDown) {
			var color:number = this.canvases.indexOf(m.target);

			this.selectedColor[color] = Math.floor(m.x);

			this.updateSwatch();
			this.recolorSliders();
		}
	}

	private updateSwatch() {
		this.swatch.setColor(this.selectedColor);
	}

	private recolorSliders() {
		for (var contextIDX:number = 0; contextIDX < this.contexts.length; contextIDX++) {
			var context:CanvasRenderingContext2D = this.contexts[contextIDX];

			for (var i:number = 0; i < this.width; i++) {
				var fillstyle:string = "rgba(";

				if (i == this.selectedColor[contextIDX]) {
					fillstyle = "rgba(0, 0, 0, 1)"; //draw black bar
				} else {
					for (var colorIDX:number = 0; colorIDX < this.colors.length; colorIDX++) {
						if (contextIDX == colorIDX) {
							fillstyle += "" + i + ", "; // hack, should be taking a percentage of width but don't need to because width == 255 LOL
						} else {
							fillstyle += "" + this.selectedColor[colorIDX] + ", ";
						}
					}

					fillstyle += "1);" // alpha
				}

				context.fillStyle = fillstyle;
				context.fillRect(i, 0, 1, this.height);
			}
		}
	}
}

class Tools {
	layers:LayerList;
	currentTool:Tool;
	tools:Tool[] = [];
	translatePoint:(p:Point) => Point;
	shortcuts:{[key: string]: Tool; } = {};
	canvas:Canvas;

	secondcolorpicker:HTMLInputElement;
	colorPicker:ColorPicker;

	firstcolorswatch:ColorSwatch;
	secondcolorswatch:ColorSwatch;

	// TODO could think of a better way to pass these around...
	constructor(layers:LayerList, tp:(p:Point) => Point, canvas:Canvas) {
		(new Keyboard()).addKeydownListener($.proxy(this.keyDown, this));

		this.layers = layers;
		this.translatePoint = tp;
		this.colorPicker = new ColorPicker();

		this.secondcolorpicker = <HTMLInputElement> document.getElementById('secondcolor');
		this.canvas = canvas;

		new MouseHandler($("#canvas-stack"), $.proxy(this.mouseMove, this), $.proxy(this.mouseDown, this), $.proxy(this.mouseUp, this));

		var firstc = $.proxy(this.firstColor, this);
		var secondc = $.proxy(this.secondColor, this);

		var that:Tools = this;
		this.tools.push(new Brush(layers, firstc, secondc, canvas));
		this.tools.push(new PaintBucket(layers, firstc, secondc, canvas));
		this.tools.push(new Eraser(layers, firstc, secondc, canvas));
		this.tools.push(new Eyedropper(layers, firstc, secondc, $.proxy(this.setFirstColor, this), canvas));

		for (var i = 0; i < this.tools.length; i++) {
			var toolName:String = this.tools[i].toolName();

			((toolName:String) => {
				$("#toolbox").append($("<div id='tool" + i + "'>" + toolName + "</div>").click(
					(e:JQueryEventObject) => {
						that.setTool(toolName);
					}
				));
			})(toolName);
		}

		for (var i = 0; i < this.tools.length; i++) {
			var iface:JQuery = this.toolInterface(this.tools[i].toolName());
			var toolName:String = iface.find("#shortcut").first().text();
			var toolNameStr:string = toolName.toString();
			toolNameStr = $.trim(toolNameStr);

			this.shortcuts[toolNameStr] = this.tools[i];
		}

		for (var i = 0; i < this.tools.length; i++) {
			this.toolInterface(this.tools[i].toolName()).hide();
		}

		this.toolInterface(this.tools[0].toolName()).show();
		this.currentTool = this.tools[0]; // ???

		this.firstcolorswatch = new ColorSwatch();
		$("#firstcolor-swatch").replaceWith(this.firstcolorswatch.$canvas);

		this.secondcolorswatch = new ColorSwatch();
		$("#secondcolor-swatch").replaceWith(this.secondcolorswatch.$canvas);
	}

	firstColor():number[] {
		return this.colorPicker.getSelectedColor();
	}

	secondColor():number[] {
		console.warn("TODO");
		return [0, 0, 0, 1];
	}

	setFirstColor(value:number[]) {
		this.colorPicker.setSelectedColor(value);
	}

	setSecondColor(value:string) {
		this.secondcolorpicker.value = value;
	}

	keyDown(key:string) {
		if (this.shortcuts[key]) {
			this.setTool(this.shortcuts[key].toolName());
		}
	}

	translateMouse(mouse:MouseInfo):MouseInfo {
		var point:Point = new Point(mouse.x, mouse.y);
		this.translatePoint(point);

		mouse.x = point.x;
		mouse.y = point.y;

		this.translatePoint(mouse.dragStartPosition);
		this.translatePoint(mouse.dragEndPosition);

		return mouse;
	}

	mouseUp(mouse:MouseInfo)   { this.currentTool.mouseUp(this.translateMouse(mouse)); }
	mouseDown(mouse:MouseInfo) { this.currentTool.mouseDown(this.translateMouse(mouse)); }
	mouseMove(mouse:MouseInfo) { this.currentTool.mouseMove(this.translateMouse(mouse)); }

	toolInterface(name:String):JQuery {
		return $("#" + name + "-interface");
	}

	setTool(toolName:String) {
		var lastTool = this.currentTool;
		var elem:JQuery = $("#whichtool");
		// I have no clue why this is required, but it is.
		(<any> elem).text(toolName);

		var toolInterface:JQuery = this.toolInterface(toolName);
		var lastToolInterface:JQuery = this.toolInterface(lastTool.toolName());

		// Order is important because you can switch to the same tool.
		lastToolInterface.hide();
		toolInterface.show();

		for (var i:number = 0; i < this.tools.length; i++) {
			if (this.tools[i].toolName() == toolName) {
				this.currentTool = this.tools[i];
			}
		}
	}
}

$(document).ready(function() {
	var c:Canvas = new Canvas();

	$("#debugging").toggle();
});
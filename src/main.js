/// <reference path="jquery.d.ts" />
/// <reference path="bootstrap.d.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CANVAS_WIDTH = 500;
var CANVAS_HEIGHT = 500;

// Add a callback option for popovers.
var tmp = $.fn.popover.Constructor.prototype.show;

function arrEq(a1, a2) {
    for (var i = 0; i < a1.length; i++) {
        if (a1[i] != a2[i])
            return false;
    }

    return true;
}

$.fn.popover.Constructor.prototype.show = function () {
    tmp.call(this);
    if (this.options.callback) {
        this.options.callback();
    }
};

var RGB = (function () {
    function RGB(r, g, b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }
    RGB.hexToRGB = function (hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? [
            parseInt(result[1], 16),
            parseInt(result[2], 16),
            parseInt(result[3], 16)
        ] : null;
    };

    RGB.rgbaStrToRGB = function (str) {
        var result = /^rgba\(([0-9]{1,3}), ([0-9]{1,3}), ([0-9]{1,3})/.exec(str);

        return result ? [parseInt(result[1]), parseInt(result[2]), parseInt(result[3])] : null;
    };

    RGB.componentToHex = function (c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    };

    RGB.rgbToHex = function (r, g, b) {
        return "#" + RGB.componentToHex(r) + RGB.componentToHex(g) + RGB.componentToHex(b);
    };
    return RGB;
})();

var Point = (function () {
    function Point(x, y) {
        this.x = x;
        this.y = y;
    }
    Point.prototype.set = function (x, y) {
        this.x = x;
        this.y = y;
    };

    Point.prototype.copy = function () {
        return new Point(this.x, this.y);
    };

    Point.stringToPoint = function (str) {
        var xy = str.split(",");

        return new Point(parseInt(xy[0]), parseInt(xy[1]));
    };

    Point.prototype.toString = function () {
        return this.x + "," + this.y;
    };
    return Point;
})();

// Currently:
//
// * Normalizes x and y positions to relative to the target element rather
//   than absolute
// * Keeps track of whether the mouse is pressed or not.
// * Keeps track of where a drag started and ended. May be the same place if
//   it's just a click.
var MouseHandler = (function () {
    function MouseHandler(elem, movecb, downcb, upcb) {
        this.x = 0;
        this.y = 0;
        this.isDown = false;
        this.dragStartPosition = new Point(0, 0);
        this.dragEndPosition = new Point(0, 0);
        this.insideElement = false;
        this.downOnElement = false;
        elem.on("mousemove", $.proxy(this.mouseMove, this));
        elem.on("mousedown", $.proxy(this.mouseDown, this));
        elem.on("mouseup", $.proxy(this.mouseUp, this));
        elem.on("mouseenter", $.proxy(this.mouseEnter, this));
        elem.on("mouseleave", $.proxy(this.mouseLeave, this));

        $(document).on("mousemove", $.proxy(this.mouseMoveOutside, this));
        $(document).on("mouseup", $.proxy(this.mouseUpOutside, this));

        this.movecb = movecb;
        this.downcb = downcb;
        this.upcb = upcb;
        this.$target = elem;
    }
    MouseHandler.prototype.mouseEnter = function (e) {
        this.insideElement = true;
    };

    MouseHandler.prototype.mouseLeave = function (e) {
        this.insideElement = false;
    };

    MouseHandler.prototype.normalize = function (e) {
        var parentOffset = this.$target.offset();

        // Normalize x/y
        this.x = e.pageX - parentOffset.left;
        this.y = e.pageY - parentOffset.top;
    };

    MouseHandler.prototype.info = function () {
        return new MouseInfo(this.x, this.y, this.isDown, this.dragStartPosition.copy(), this.dragEndPosition.copy(), this.$target);
    };

    MouseHandler.prototype.mouseMove = function (e) {
        this.normalize(e);

        if (this.isDown) {
            this.dragEndPosition.set(this.x, this.y);
        }

        if (this.movecb)
            this.movecb(this.info());
    };

    MouseHandler.prototype.mouseMoveOutside = function (e) {
        if (this.downOnElement && !this.insideElement) {
            this.mouseMove(e);
        }
    };

    MouseHandler.prototype.mouseUpOutside = function (e) {
        this.normalize(e);

        this.isDown = false;
        this.downOnElement = false;
    };

    MouseHandler.prototype.mouseDown = function (e) {
        this.normalize(e);

        this.isDown = true;
        this.downOnElement = this.insideElement;
        this.dragStartPosition.set(this.x, this.y);

        if (this.downcb)
            this.downcb(this.info());
    };

    MouseHandler.prototype.mouseUp = function (e) {
        this.normalize(e);

        this.isDown = false;
        this.downOnElement = false;
        this.dragEndPosition.set(this.x, this.y);

        if (this.upcb)
            this.upcb(this.info());
    };
    return MouseHandler;
})();

var MouseInfo = (function () {
    function MouseInfo(x, y, isDown, dragStartPosition, dragEndPosition, target) {
        this.x = 0;
        this.y = 0;
        this.isDown = false;
        this.dragStartPosition = new Point(0, 0);
        this.dragEndPosition = new Point(0, 0);
        this.x = x;
        this.y = y;
        this.isDown = isDown;
        this.dragStartPosition = dragStartPosition;
        this.dragEndPosition = dragEndPosition;
        this.target = target;
    }
    return MouseInfo;
})();

var Keyboard = (function () {
    function Keyboard() {
        this.downCallback = undefined;
        this.upCallback = undefined;
        $(document).on("keydown", $.proxy(this.keyDown, this));
        $(document).on("keyup", $.proxy(this.keyUp, this));
    }
    Keyboard.prototype.getKey = function (e) {
        var key = String.fromCharCode(e.which).toLowerCase();
        if (e.shiftKey) {
            key = key.toUpperCase();
        }

        return key;
    };

    Keyboard.prototype.keyDown = function (e) {
        if (this.downCallback != undefined) {
            this.downCallback(this.getKey(e));
        }

        Keyboard.shiftKey = e.shiftKey;
    };

    Keyboard.prototype.keyUp = function (e) {
        if (this.upCallback != undefined) {
            this.upCallback(this.getKey(e));
        }

        Keyboard.shiftKey = e.shiftKey;
    };

    Keyboard.prototype.addKeydownListener = function (callback) {
        this.downCallback = callback;

        return this;
    };

    Keyboard.prototype.addKeyupListener = function (callback) {
        this.upCallback = callback;

        return this;
    };
    Keyboard.shiftKey = false;
    return Keyboard;
})();

var Preview = (function () {
    function Preview(canvas) {
        this.dirty = false;
        this.zoomWindowX = 0;
        this.zoomWindowY = 0;
        this.zoomWindowWidth = 0;
        this.zoomWindowHeight = 0;
        if (Preview.previewWindow) {
            console.error("There should only be one Preview window.");
        }

        Preview.previewWindow = this;

        this.preview = document.getElementById('preview');

        this.previewContext = this.preview.getContext('2d');

        var update = $.proxy(this.updateMouse, this);

        this.mouse = new MouseHandler($(this.preview), update, update, update);
        this.canvas = canvas;
    }
    Preview.prototype.updateMouse = function (mouse) {
        if (mouse.isDown) {
            this.moveZoomWindow(mouse.x - this.zoomWindowWidth / 2, mouse.y - this.zoomWindowHeight / 2);
        }
    };

    Preview.prototype.moveZoomWindow = function (x, y) {
        this.zoomWindowX = x;
        this.zoomWindowY = y;

        if (this.zoomWindowX < 0)
            this.zoomWindowX = 0;
        if (this.zoomWindowY < 0)
            this.zoomWindowY = 0;

        if (this.zoomWindowX + this.zoomWindowWidth > this.preview.width)
            this.zoomWindowX = this.preview.width - this.zoomWindowWidth;
        if (this.zoomWindowY + this.zoomWindowHeight > this.preview.height)
            this.zoomWindowY = this.preview.height - this.zoomWindowHeight;

        this.dirty = true;
    };

    Preview.prototype.update = function () {
        var img = this.canvas.renderImage();
        this.previewContext.fillStyle = "white";
        this.previewContext.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

        this.previewContext.drawImage(img.content, 0, 0);

        img.remove();

        // draw zoom window
        this.zoomWindowWidth = this.canvas.width / this.canvas.scale;
        this.zoomWindowHeight = this.canvas.height / this.canvas.scale;

        this.previewContext.strokeRect(this.zoomWindowX, this.zoomWindowY, this.zoomWindowWidth, this.zoomWindowHeight);
    };
    return Preview;
})();

var UndoStep = (function () {
    function UndoStep(data, toolName) {
        this.data = data;
        this.toolName = toolName;
    }
    return UndoStep;
})();

var Layer = (function () {
    function Layer(name) {
        this.name = name;
        this.graphic = new Graphic();
    }
    return Layer;
})();

var LayerList = (function () {
    function LayerList(canvas) {
        var _this = this;
        this.list = [];
        this.selectedLayer = 0;
        var $layerDiv = $('#layers');
        var $newLayer = $('<a/>', { html: "Add layer", href: "#" });
        this.$list = $('<ol/>');
        this.canvas = canvas;

        $layerDiv.append($('<div/>', {
            html: 'Layers:'
        }).css('font-weight', 'thick')).append(this.$list).append($newLayer);

        $newLayer.click(function () {
            _this.addLayer(true);
        });

        this.addLayer(false);

        this.changeSelectedLayer(0);
    }
    LayerList.prototype.getSelectedLayer = function () {
        return this.list[this.selectedLayer];
    };

    LayerList.prototype.changeSelectedLayer = function (which) {
        var oldLayer = this.list[this.selectedLayer];
        var newLayer = this.list[which];

        this.selectedLayer = which;
        oldLayer.$listDiv.removeClass("selected");
        newLayer.$listDiv.addClass("selected");
    };

    LayerList.prototype.addLayer = function (inputName) {
        var layerName = "Layer " + this.list.length;
        var layerNumber = this.list.length;
        var l = new Layer(layerName);
        this.list.push(l);

        this.addLayerToDocument(layerName, inputName, l, layerNumber);

        this.canvas.addLayer(l);
    };

    LayerList.prototype.makeLayerToolbar = function (layer, layerDiv) {
        var _this = this;
        var result = $('<span/>');

        var $remove = $("<a/>", { href: "#", html: "[X]" }).mousedown(function () {
            _this.list.splice(_this.list.indexOf(layer), 1);
            layerDiv.remove();
            layer.graphic.remove();

            _this.canvas.removeLayer(layer);
        });

        var $hideThis = $("<input/>", { type: "checkbox" }).mousedown(function () {
            layer.graphic.toggleVisible();
            _this.canvas.render();
        });

        var $hideOthers = $("<a/>", { href: "#", html: "hide others" }).mousedown(function () {
            console.log("hide others pressed");
        });

        result.append($remove).append(" ").append($hideThis).append(" ").append($hideOthers);

        return result;
    };

    LayerList.prototype.addLayerToDocument = function (layerName, inputName, newLayer, layerNumber) {
        var _this = this;
        var $listItem = $('<li/>');
        var $listDiv = $('<div/>', { html: layerName }).append(" ");
        var $layerTools = this.makeLayerToolbar(newLayer, $listItem);

        $listDiv.append($layerTools).dblclick(function () {
            $listDiv.replaceWith($listText);
            $listText.focus();
            $listText.select();
        }).click(function () {
            _this.changeSelectedLayer(layerNumber);
        });

        var $listText = $('<input/>', { type: "text" }).val(layerName).keypress(function (e) {
            if (e.which == 13) {
                $listDiv.html($listText.val());
                $listText.replaceWith($listDiv);
                layerName = $listText.val();
            }
        });

        $listItem.append($listDiv);
        this.$list.append($listItem);

        if (inputName) {
            $listItem.trigger("dblclick");
            $listText.focus();
        }

        newLayer.$listDiv = $listDiv;
        newLayer.$listText = $listText;
        newLayer.$listItem = $listItem;
    };

    LayerList.prototype.removeLayer = function () {
        console.log("TODO");
    };
    return LayerList;
})();

var UndoHistory = (function () {
    function UndoHistory(canvas) {
        this.stack = [];
        this.currentStep = 0;
        this.canvas = canvas;
    }
    UndoHistory.prototype.addStep = function (data, toolName) {
        var _this = this;
        var step = this.stack.length;
        var that = this;
        var undoStep = new UndoStep(data, toolName);
        var $elem;

        var popoverContent = function () {
            var canvas = document.createElement('canvas');
            canvas.width = CANVAS_WIDTH;
            canvas.height = CANVAS_HEIGHT;
            canvas.getContext('2d').putImageData(_this.stack[step].data.context.getImageData(0, 0, 500, 500), 0, 0);

            return canvas;
        };

        $elem = $('<li/>', {
            html: $('<a/>', {
                href: '#',
                text: toolName,
                title: "Preview"
            }).popover({
                trigger: "hover",
                html: true,
                content: popoverContent
            }),
            'data-content': "tooltip"
        }).click(function (e) {
            that.revertTo(step);
        }).appendTo("#undo-list");

        undoStep.$elem = $elem;

        if (this.currentStep != this.stack.length) {
            for (var i = this.currentStep + 1; i < this.stack.length; i++) {
                this.stack[i].$elem.remove();
            }

            this.currentStep = this.stack.length;
        }

        ++this.currentStep;
        this.stack.push(undoStep);
    };

    UndoHistory.prototype.revertTo = function (whichStep) {
        Graphic.undo(whichStep, this.canvas.displayList);

        for (var i = 0; i < this.stack.length; i++) {
            var newColor;
            if (i <= whichStep) {
                newColor = "blue";
            } else {
                newColor = "lightgray";
            }

            this.stack[i].$elem.children("a").css("color", newColor);
        }

        this.currentStep = whichStep;
    };
    return UndoHistory;
})();

// This represents anything you can draw on.
var Graphic = (function () {
    function Graphic(canvas) {
        if (typeof canvas === "undefined") { canvas = null; }
        this.dirty = false;
        this.undoStack = [];
        this.visible = true;
        this.x = 0;
        this.y = 0;
        this.locked = false;
        this.fixed = false;
        this.changed = false;
        if (canvas == null) {
            this.content = $("<canvas>").attr("width", CANVAS_WIDTH).attr("height", CANVAS_HEIGHT)[0];
            this.context = this.content.getContext("2d");
        } else {
            this.content = canvas;
            this.context = this.content.getContext("2d");
        }

        this.masterContent = $("<canvas>").attr("width", CANVAS_WIDTH).attr("height", CANVAS_HEIGHT)[0];
        this.masterContext = this.masterContent.getContext("2d");

        this.context.globalAlpha = 1;

        this.context.fillStyle = "rgba(255,255,255,0)";
        this.context.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

        this.context.webkitImageSmoothingEnabled = false;
        this.context.mozImageSmoothingEnabled = false;

        this.width = CANVAS_WIDTH;
        this.height = CANVAS_HEIGHT;

        Graphic.displayList.push(this);

        if (!Graphic.hasInitialized) {
            Graphic.hasInitialized = true;

            var updateGfx = (function () {
                var dc = 0;

                for (var i = 0; i < Graphic.displayList.length; i++) {
                    var graphic = Graphic.displayList[i];

                    if (!graphic.dirty && !Preview.previewWindow.dirty)
                        continue;

                    ++dc;

                    if (graphic.fixed) {
                        graphic.updateGraphic(graphic.x, graphic.y);
                    } else {
                        graphic.updateGraphic(-Preview.previewWindow.zoomWindowX + graphic.x, -Preview.previewWindow.zoomWindowY + graphic.y);
                    }
                }

                $("#dirty-count").text("Dirty: " + dc);
                $("#canvas-count").text("Canvases: " + Graphic.displayList.length);

                Preview.previewWindow.update();
                Preview.previewWindow.dirty = false;

                window.requestAnimationFrame(updateGfx);
            });

            window.requestAnimationFrame(updateGfx);
        }
        ;

        this.updateGraphic();
    }
    Graphic.addUndoStep = function (list) {
        for (var i = 0; i < list.length; i++) {
            var graphic = list[i];

            if (graphic.changed) {
                graphic.undoStack.push(graphic.clone());

                graphic.changed = false;
            } else {
                graphic.undoStack.push(graphic.undoStack[list.length - 1]); // unchanged, so use same graphic.
            }
        }
    };

    Graphic.undo = function (steps, list) {
        for (var i = 0; i < list.length; i++) {
            var graphic = list[i];

            graphic.clear();
            graphic.undoStack[steps].renderTo(graphic, 0, 0);
        }
    };

    Graphic.prototype.remove = function () {
        var index = Graphic.displayList.indexOf(this);
        Graphic.displayList.splice(index, 1);
    };

    // needs to be called from requestAnimationFrame callback.
    Graphic.prototype.updateGraphic = function (offsetX, offsetY) {
        if (typeof offsetX === "undefined") { offsetX = 0; }
        if (typeof offsetY === "undefined") { offsetY = 0; }
        if (this.locked) {
            console.error("called updateGraphic while locked");
            return;
        }

        this.context.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

        if (this.visible) {
            this.context.drawImage(this.masterContent, offsetX, offsetY);
        }

        this.cachedImageData = this.masterContext.getImageData(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        this.cachedImageDataData = this.cachedImageData.data;
        this.dirty = false;
        this.changed = true;
    };

    Graphic.prototype.clear = function () {
        this.dirty = true;

        this.masterContext.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    };

    Graphic.prototype.clearRect = function (x, y, w, h) {
        this.dirty = true;

        this.masterContext.clearRect(x, y, w, h);
    };

    // call before doing a lot of intensive graphics work. will optimize all setPixel() calls.
    Graphic.prototype.lock = function () {
        this.locked = true;
    };

    // call when you've finished doing your work.
    Graphic.prototype.unlock = function () {
        this.locked = false;

        this.masterContext.putImageData(this.cachedImageData, 0, 0);
    };

    // Note that this is different from html5 canvas scaling in that it retroactively scales everything you've already drawn.
    // That seems like expected behavior anyways...
    Graphic.prototype.scale = function (x, y) {
        this.dirty = true;

        // reset scale to (1, 1)
        this.context.setTransform(1, 0, 0, 1, 0, 0);
        this.context.scale(x, y);
    };

    Graphic.prototype.renderTo = function (dest, offsetX, offsetY) {
        dest.dirty = true;

        dest.masterContext.drawImage(this.content, offsetX, offsetY);
    };

    Graphic.prototype.clone = function () {
        var result = new Graphic();
        this.renderTo(result, 0, 0);
        result.updateGraphic(0, 0);

        return result;
    };

    Graphic.prototype.renderToUnscaled = function (dest, offsetX, offsetY) {
        dest.dirty = true;

        dest.masterContext.drawImage(this.masterContent, offsetX, offsetY);
    };

    Graphic.prototype.setVisible = function (v) {
        this.dirty = true;

        this.visible = v;
    };

    Graphic.prototype.toggleVisible = function () {
        this.dirty = true;

        this.visible = !this.visible;
    };

    Graphic.prototype.isVisible = function () {
        this.dirty = true;

        return this.visible;
    };

    Graphic.prototype.contains = function (p) {
        return p.x >= 0 && p.x < CANVAS_WIDTH && p.y >= 0 && p.y < CANVAS_HEIGHT;
    };

    Graphic.prototype.setPixel = function (x, y, color) {
        this.dirty = true;

        if (!this.locked) {
            this.masterContext.fillStyle = RGB.rgbToHex(color[0], color[1], color[2]);
            this.masterContext.fillRect(x, y, 1, 1);
        } else {
            var offset = 2000 * y + x * 4;

            this.cachedImageDataData[offset + 0] = 255;
            this.cachedImageDataData[offset + 1] = 0;
            this.cachedImageDataData[offset + 2] = 255;
            this.cachedImageDataData[offset + 3] = 1;
        }
    };

    /*
    private setPixelHaxor(x:number, y:number) {
    var offset = this.content.height * 4 * y + x * 4;
    
    if (!this.imgData) {
    this.imgData = this.context.getImageData(0,0,this.content.width,this.content.height).data;
    }
    
    this.imgData[0 + offset] = 0;
    this.imgData[1 + offset] = 0;
    this.imgData[2 + offset] = 0;
    this.imgData[3 + offset] = 0;
    }
    */
    Graphic.prototype.xyToIDOffset = function (x, y) {
        return this.content.width * 4 * y + x * 4;
    };

    Graphic.prototype.getPixel = function (x, y) {
        var offset = this.content.width * 4 * y + x * 4;
        var data = [
            this.cachedImageDataData[0 + offset],
            this.cachedImageDataData[1 + offset],
            this.cachedImageDataData[2 + offset],
            this.cachedImageDataData[3 + offset]];
        return data;
    };

    Graphic.prototype.fillRect = function (x, y, width, height, color) {
        this.dirty = true;

        this.masterContext.fillStyle = RGB.rgbToHex(color[0], color[1], color[2]);
        this.masterContext.fillRect(x, y, width, height);
    };

    Graphic.prototype.strokeRect = function (x, y, width, height, color) {
        this.dirty = true;

        this.masterContext.fillStyle = RGB.rgbToHex(color[0], color[1], color[2]);
        this.masterContext.strokeRect(x, y, width, height);
    };
    Graphic.displayList = [];
    Graphic.hasInitialized = false;
    return Graphic;
})();

// Generic tool class. Takes a setPixel callback function that Canvas
// implements so it doesn't have to get tied up in graphic logic.
// TODO: Don't like how you have to pass in translate currently.
var Tool = (function () {
    function Tool(layers, firstColor, secondColor, canvas) {
        this.layers = layers;
        this.firstColor = firstColor;
        this.secondColor = secondColor;
        this.canvas = canvas;
    }
    // There is no protected keyword O_O
    Tool.prototype.mouseUp = function (mouse) {
        if (this.addsUndoStep()) {
            this.canvas.addUndoStep(this.toolName()); // seems like a bug in TypeScript?
        }
    };
    Tool.prototype.mouseDown = function (mouse) {
    };
    Tool.prototype.mouseMove = function (mouse) {
    };
    Tool.prototype.toolName = function () {
        return "Oops!";
    };

    Tool.prototype.getGraphic = function () {
        return this.layers.getSelectedLayer().graphic;
    };

    // some handy methods so you don't have to constantly call getGraphic().
    Tool.prototype.getPixel = function (x, y) {
        return this.getGraphic().getPixel(x, y);
    };
    Tool.prototype.setPixel = function (x, y, color) {
        this.getGraphic().setPixel(x, y, color);
    };

    Tool.prototype.showsGhost = function () {
        return true;
    };

    Tool.prototype.addsUndoStep = function () {
        return true;
    };
    return Tool;
})();

var Brush = (function (_super) {
    __extends(Brush, _super);
    function Brush() {
        _super.apply(this, arguments);
        this.oldPoint = null;
        this.currentGraphic = null;
        this.currentStraightGraphic = null;
    }
    Brush.prototype.getGraphic = function () {
        if (Keyboard.shiftKey) {
            return this.currentStraightGraphic;
        } else {
            return this.currentGraphic;
        }
    };

    Brush.prototype.mouseUp = function (mouse) {
        var currentPoint = new Point(mouse.x, mouse.y);

        this.drawLine(this.oldPoint.x, this.oldPoint.y, currentPoint.x, currentPoint.y, this.firstColor());
        this.oldPoint = null;

        var target = this.layers.getSelectedLayer().graphic;

        if (Keyboard.shiftKey) {
            this.currentStraightGraphic.renderToUnscaled(target, 0, 0);
        } else {
            this.currentGraphic.renderToUnscaled(target, 0, 0);
        }

        this.canvas.removeGraphic(this.currentGraphic);
        this.canvas.removeGraphic(this.currentStraightGraphic);

        _super.prototype.mouseUp.call(this, mouse);
    };

    // Draw a line with no anti-aliasing.
    Brush.prototype.drawLine = function (startX, startY, endX, endY, color) {
        var dx = endX - startX;
        var dy = endY - startY;

        var mag = Math.sqrt(dx * dx + dy * dy);

        dx /= mag;
        dy /= mag;

        var xSteps = Math.ceil((endX - startX) / dx);
        var ySteps = Math.ceil((endY - startY) / dy);

        if (isNaN(xSteps))
            xSteps = 0;
        if (isNaN(ySteps))
            ySteps = 0;

        var steps = Math.max(xSteps, ySteps, 1);
        var currentX = startX;
        var currentY = startY;

        for (var i = 0; i < steps; i++) {
            var x = Math.floor(currentX);
            var y = Math.floor(currentY);

            this.setPixel(x, y, color);

            currentX += dx;
            currentY += dy;
        }
    };

    Brush.prototype.setPixel = function (x, y, color) {
        var size = this.getSize();
        var halfsize = Math.floor(this.getSize() / 2);
        var halfsize2 = Math.ceil(this.getSize() / 2);

        for (var i = x - halfsize; i < x + halfsize2; i++) {
            for (var j = y - halfsize; j < y + halfsize2; j++) {
                _super.prototype.setPixel.call(this, i, j, color);
            }
        }

        ColorPalette.colorPalette.addColor(color);
    };

    Brush.prototype.getSize = function () {
        return parseInt($("#brush-size").val());
    };

    Brush.prototype.mouseDown = function (mouse) {
        this.currentGraphic = new Graphic();
        this.currentStraightGraphic = new Graphic();

        this.currentGraphic.scale(this.canvas.scale, this.canvas.scale);
        this.currentStraightGraphic.scale(this.canvas.scale, this.canvas.scale);

        this.canvas.addGraphic(this.currentGraphic);
        this.canvas.addGraphic(this.currentStraightGraphic);

        this.oldPoint = new Point(mouse.x, mouse.y);
    };

    Brush.prototype.mouseMove = function (mouse) {
        if (mouse.isDown) {
            var currentPoint = new Point(mouse.x, mouse.y);

            if (Keyboard.shiftKey) {
                this.currentStraightGraphic.visible = true;
                this.currentGraphic.visible = false;
            } else {
                this.currentStraightGraphic.visible = false;
                this.currentGraphic.visible = true;
            }

            if (this.currentGraphic.visible) {
                if (this.oldPoint != null) {
                    this.drawLine(this.oldPoint.x, this.oldPoint.y, currentPoint.x, currentPoint.y, this.firstColor());
                }
            } else if (this.currentStraightGraphic.visible) {
                this.currentStraightGraphic.clear();

                if (Math.abs(mouse.dragStartPosition.x - mouse.dragEndPosition.x) > Math.abs(mouse.dragStartPosition.y - mouse.dragEndPosition.y)) {
                    this.drawLine(mouse.dragStartPosition.x, mouse.dragStartPosition.y, mouse.dragEndPosition.x, mouse.dragStartPosition.y, this.firstColor());
                } else {
                    this.drawLine(mouse.dragStartPosition.x, mouse.dragStartPosition.y, mouse.dragStartPosition.x, mouse.dragEndPosition.y, this.firstColor());
                }
            }

            this.oldPoint = currentPoint;
        }
    };

    Brush.prototype.toolName = function () {
        return "Brush";
    };
    return Brush;
})(Tool);

var Eyedropper = (function (_super) {
    __extends(Eyedropper, _super);
    function Eyedropper(layers, firstColor, secondColor, setFirstColor, canvas) {
        _super.call(this, layers, firstColor, secondColor, canvas);
        this.setFirstColor = setFirstColor;

        this.canvas = canvas;
    }
    Eyedropper.prototype.showsGhost = function () {
        return false;
    };

    Eyedropper.prototype.mouseDown = function (mouse) {
        var g = this.canvas.renderImage();

        this.setFirstColor(g.getPixel(mouse.x, mouse.y));

        g.remove();
    };

    Eyedropper.prototype.toolName = function () {
        return "Eyedropper";
    };

    Eyedropper.prototype.addsUndoStep = function () {
        return false;
    };
    return Eyedropper;
})(Tool);

var PaintBucket = (function (_super) {
    __extends(PaintBucket, _super);
    function PaintBucket() {
        _super.apply(this, arguments);
    }
    PaintBucket.prototype.toolName = function () {
        return "PaintBucket";
    };

    PaintBucket.prototype.mouseDown = function (mouse) {
        var p = new Point(mouse.x, mouse.y);
        var color = this.firstColor();
        var tried = {};
        tried[p.toString()] = true;

        var justTried = [p];
        var oldColor = this.getPixel(p.x, p.y);

        this.setPixel(p.x, p.y, color);

        var neighborDX = [0, 0, -1, 1];
        var neighborDY = [1, -1, 0, 0];

        while (justTried.length != 0) {
            var outerEdgeOfFill = [];

            for (var i = 0; i < justTried.length; i++) {
                var currentPoint = justTried[i];

                for (var j = 0; j < neighborDY.length; j++) {
                    var dx = neighborDX[j];
                    var dy = neighborDY[j];
                    var n = new Point(currentPoint.x + dx, currentPoint.y + dy);

                    if (!this.getGraphic().contains(n))
                        continue;
                    if (!arrEq(this.getPixel(n.x, n.y), oldColor))
                        continue;
                    if (tried[n.toString()])
                        continue;

                    outerEdgeOfFill.push(n);
                    tried[n.toString()] = true;
                    this.setPixel(n.x, n.y, color);
                }
            }

            justTried = outerEdgeOfFill;
        }
    };
    return PaintBucket;
})(Tool);

var Eraser = (function (_super) {
    __extends(Eraser, _super);
    function Eraser(layers, firstColor, secondColor, canvas) {
        _super.call(this, layers, secondColor, secondColor, canvas);
    }
    Eraser.prototype.toolName = function () {
        return "Eraser";
    };
    return Eraser;
})(Brush);

//TODO probably should separate input from gfx.
// Probably shouldn't do new Preview(), from a philosophical point of view.
var Canvas = (function () {
    function Canvas() {
        this.width = CANVAS_WIDTH;
        this.height = CANVAS_HEIGHT;
        this.scale = 1.0;
        this.displayList = [];
        this.undoHistory = new UndoHistory(this);
        this.canvasBoundary = new Graphic($("#topcanvas")[0]);
        this.makeTransparentBackground();
        this.mouse = new MouseHandler($("#canvas-stack"), $.proxy(this.mouseMove, this), $.proxy(this.mouseDown, this), $.proxy(this.mouseUp, this));
        this.keyboard = new Keyboard().addKeydownListener($.proxy(this.keyDown, this));
        this.layers = new LayerList(this);
        this.tools = new Tools(this.layers, $.proxy(this.translatePoint, this), this);
        this.preview = new Preview(this);
        this.addUndoStep("New Document");

        this.ghost = new Graphic();
        this.ghost.setPixel(0, 0, [0, 0, 0, 1]);
        this.addGraphic(this.ghost);
        new ColorPalette(this, this.tools);
    }
    Canvas.prototype.addLayer = function (l) {
        l.graphic.scale(this.scale, this.scale);

        this.addGraphic(l.graphic);
    };

    Canvas.prototype.removeLayer = function (l) {
        this.removeGraphic(l.graphic);
        l.graphic.remove();
    };

    Canvas.prototype.makeTransparentBackground = function () {
        var blockSize = 15;

        this.transparentBG = new Graphic($("#bg")[0]);
        this.transparentBG.fixed = true;

        for (var i = 0; i < this.width; i += blockSize) {
            for (var j = 0; j < this.width; j += blockSize) {
                if (((i / blockSize) + (j / blockSize)) % 2 == 0) {
                    this.transparentBG.fillRect(i, j, blockSize, blockSize, [200, 200, 200, 1]);
                }
            }
        }
    };

    Canvas.prototype.makeCanvasBoundary = function () {
        var effectiveWidth = this.width * this.scale;
        var effectiveHeight = this.height * this.scale;

        this.canvasBoundary.fillRect(0, 0, this.canvasBoundary.width, this.canvasBoundary.height, [80, 80, 80, 1]);
        this.canvasBoundary.clearRect(0, 0, effectiveWidth, effectiveHeight);
        this.canvasBoundary.strokeRect(0, 0, effectiveWidth, effectiveHeight, [0, 0, 0, 1]);
    };

    Canvas.prototype.addGraphic = function (g) {
        this.displayList.push(g);

        var $canvas = $(g.content);
        $canvas.css({ "position": "absolute", "z-index": "" + this.displayList.length }).attr("id", "layer" + this.displayList.length);
        $("#canvas-stack").append($canvas);
    };

    Canvas.prototype.removeGraphic = function (g) {
        var index = this.displayList.indexOf(g);

        if (index == -1) {
            console.error("Couldn't find graphic to remove.");
            return;
        }

        this.displayList.splice(index, 1);
        $(g.content).remove();
    };

    Canvas.prototype.render = function () {
    };

    Canvas.prototype.zoom = function (delta) {
        this.scale += delta;

        for (var i = 0; i < this.displayList.length; i++) {
            this.displayList[i].scale(this.scale, this.scale);
        }
    };

    Canvas.prototype.keyDown = function (key) {
        if (key == "z") {
            this.zoom(0.1);
        }

        if (key == "Z") {
            this.zoom(-0.1);
        }

        if (key == "1") {
            $("#debugging").toggle();
        }

        this.makeCanvasBoundary();
    };

    Canvas.prototype.renderImage = function () {
        var renderImageGraphic = new Graphic();

        for (var i = 0; i < this.displayList.length; i++) {
            this.displayList[i].updateGraphic(0, 0);
            this.displayList[i].renderToUnscaled(renderImageGraphic, 0, 0);
        }

        renderImageGraphic.updateGraphic();

        return renderImageGraphic;
    };

    Canvas.prototype.addUndoStep = function (toolName) {
        if (this.tools.currentTool.addsUndoStep()) {
            this.undoHistory.addStep(this.renderImage(), toolName);

            Graphic.addUndoStep(this.displayList);
        }
    };

    Canvas.prototype.translatePoint = function (pt) {
        pt.x /= this.scale;
        pt.y /= this.scale;

        pt.x += this.preview.zoomWindowX;
        pt.y += this.preview.zoomWindowY;

        pt.x = Math.floor(pt.x);
        pt.y = Math.floor(pt.y);

        return pt;
    };

    Canvas.prototype.mouseMove = function (mouse) {
        var pt = new Point(mouse.x, mouse.y);
        pt = this.translatePoint(pt);

        this.ghost.x = pt.x;
        this.ghost.y = pt.y;
    };
    return Canvas;
})();

var ColorSwatch = (function () {
    function ColorSwatch(color) {
        if (typeof color === "undefined") { color = [255, 0, 0]; }
        this.color = [255, 0, 0];
        this.size = 10;
        this.color = color;
        this.$canvas = $("<canvas/>", { "title": " lalalal" });
        this.$canvas[0].width = this.size;
        this.$canvas[0].height = this.size;
        this.ctx = this.$canvas[0].getContext("2d");

        this.$canvas.tooltip();

        var that = this;

        this.$canvas.on("mouseover", function () {
            that.$canvas.css({ "border-color": "#ccc" });
        }).on("mouseout", function () {
            that.$canvas.css({ "border-color": "#000" });
        });

        this.updateSwatch();
    }
    ColorSwatch.prototype.onClick = function (f) {
        this.$canvas.on("click", f);
    };

    ColorSwatch.prototype.updateSwatch = function () {
        this.ctx.fillStyle = "rgba(" + this.color[0] + ", " + this.color[1] + ", " + this.color[2] + ", 1)";
        this.ctx.fillRect(0, 0, this.size, this.size);

        // update the tooltip
        this.$canvas.attr("data-original-title", "RGB " + this.color[0] + ", " + this.color[1] + ", " + this.color[2]);
    };

    ColorSwatch.prototype.setColor = function (color) {
        for (var i = 0; i < color.length; i++) {
            if (color[i] < 0)
                color[i] = 0;
            if (color[i] > 255)
                color[i] = 255;
        }
        this.color = color;

        this.updateSwatch();
    };

    ColorSwatch.prototype.getCanvas = function () {
        return this.$canvas;
    };
    return ColorSwatch;
})();

var ColorPalette = (function () {
    function ColorPalette(c, t) {
        this.colors = {};
        this.colorCounts = { "0 0 0": 500 * 500 };
        ColorPalette.colorPalette = this;

        this.t = t;
        this.canvas = c;
        this.addColor([0, 0, 0]);
    }
    ColorPalette.prototype.addColor = function (rgb) {
        var color = rgb.join(" ");
        var that = this;
        var arrClone = [rgb[0], rgb[1], rgb[2], rgb[3]];

        if (!this.colors[color]) {
            this.colors[color] = true;
            var newSwatch = new ColorSwatch(rgb);
            $("#palette").append(newSwatch.getCanvas());

            newSwatch.onClick(function () {
                that.t.setFirstColor(arrClone);
            });
        }
    };
    return ColorPalette;
})();

var ColorPicker = (function () {
    function ColorPicker() {
        this.colors = ["r", "g", "b"];
        this.contexts = [];
        this.canvases = [];
        this.selectedColor = [50, 200, 50];
        this.width = 255;
        this.height = 10;
        var mouseUpdateProxy = $.proxy(this.mouseUpdate, this);

        for (var i = 0; i < this.colors.length; i++) {
            var $canvas = $("#picker-" + this.colors[i]);
            var canvas = $canvas[0];

            canvas.width = this.width;
            canvas.height = this.height;

            new MouseHandler($canvas, mouseUpdateProxy, mouseUpdateProxy, mouseUpdateProxy);

            this.canvases.push($canvas);
            this.contexts.push(canvas.getContext("2d"));
        }

        this.swatch = new ColorSwatch();
        $("#picker-swatch").replaceWith(this.swatch.$canvas);

        this.updateSwatch();
        this.recolorSliders();
    }
    ColorPicker.prototype.getSelectedColor = function () {
        return this.selectedColor;
    };

    ColorPicker.prototype.setSelectedColor = function (color) {
        this.selectedColor = color;

        this.updateSwatch();
        this.recolorSliders();
    };

    ColorPicker.prototype.mouseUpdate = function (m) {
        if (m.isDown) {
            var color = this.canvases.indexOf(m.target);

            this.selectedColor[color] = Math.floor(m.x);

            this.updateSwatch();
            this.recolorSliders();
        }
    };

    ColorPicker.prototype.updateSwatch = function () {
        this.swatch.setColor(this.selectedColor);
    };

    ColorPicker.prototype.recolorSliders = function () {
        for (var contextIDX = 0; contextIDX < this.contexts.length; contextIDX++) {
            var context = this.contexts[contextIDX];

            for (var i = 0; i < this.width; i++) {
                var fillstyle = "rgba(";

                if (i == this.selectedColor[contextIDX]) {
                    fillstyle = "rgba(0, 0, 0, 1)"; //draw black bar
                } else {
                    for (var colorIDX = 0; colorIDX < this.colors.length; colorIDX++) {
                        if (contextIDX == colorIDX) {
                            fillstyle += "" + i + ", "; // hack, should be taking a percentage of width but don't need to because width == 255 LOL
                        } else {
                            fillstyle += "" + this.selectedColor[colorIDX] + ", ";
                        }
                    }

                    fillstyle += "1);";
                }

                context.fillStyle = fillstyle;
                context.fillRect(i, 0, 1, this.height);
            }
        }
    };
    return ColorPicker;
})();

var Tools = (function () {
    // TODO could think of a better way to pass these around...
    function Tools(layers, tp, canvas) {
        this.tools = [];
        this.shortcuts = {};
        (new Keyboard()).addKeydownListener($.proxy(this.keyDown, this));

        this.layers = layers;
        this.translatePoint = tp;
        this.colorPicker = new ColorPicker();

        this.secondcolorpicker = document.getElementById('secondcolor');
        this.canvas = canvas;

        new MouseHandler($("#canvas-stack"), $.proxy(this.mouseMove, this), $.proxy(this.mouseDown, this), $.proxy(this.mouseUp, this));

        var firstc = $.proxy(this.firstColor, this);
        var secondc = $.proxy(this.secondColor, this);

        var that = this;
        this.tools.push(new Brush(layers, firstc, secondc, canvas));
        this.tools.push(new PaintBucket(layers, firstc, secondc, canvas));
        this.tools.push(new Eraser(layers, firstc, secondc, canvas));
        this.tools.push(new Eyedropper(layers, firstc, secondc, $.proxy(this.setFirstColor, this), canvas));

        for (var i = 0; i < this.tools.length; i++) {
            var toolName = this.tools[i].toolName();

            (function (toolName) {
                $("#toolbox").append($("<div id='tool" + i + "'>" + toolName + "</div>").click(function (e) {
                    that.setTool(toolName);
                }));
            })(toolName);
        }

        for (var i = 0; i < this.tools.length; i++) {
            var iface = this.toolInterface(this.tools[i].toolName());
            var toolName = iface.find("#shortcut").first().text();
            var toolNameStr = toolName.toString();
            toolNameStr = $.trim(toolNameStr);

            this.shortcuts[toolNameStr] = this.tools[i];
        }

        for (var i = 0; i < this.tools.length; i++) {
            this.toolInterface(this.tools[i].toolName()).hide();
        }

        this.toolInterface(this.tools[0].toolName()).show();
        this.currentTool = this.tools[0]; // ???

        this.firstcolorswatch = new ColorSwatch();
        $("#firstcolor-swatch").replaceWith(this.firstcolorswatch.$canvas);

        this.secondcolorswatch = new ColorSwatch();
        $("#secondcolor-swatch").replaceWith(this.secondcolorswatch.$canvas);
    }
    Tools.prototype.firstColor = function () {
        return this.colorPicker.getSelectedColor();
    };

    Tools.prototype.secondColor = function () {
        console.warn("TODO");
        return [0, 0, 0, 1];
    };

    Tools.prototype.setFirstColor = function (value) {
        this.colorPicker.setSelectedColor(value);
    };

    Tools.prototype.setSecondColor = function (value) {
        this.secondcolorpicker.value = value;
    };

    Tools.prototype.keyDown = function (key) {
        if (this.shortcuts[key]) {
            this.setTool(this.shortcuts[key].toolName());
        }
    };

    Tools.prototype.translateMouse = function (mouse) {
        var point = new Point(mouse.x, mouse.y);
        this.translatePoint(point);

        mouse.x = point.x;
        mouse.y = point.y;

        this.translatePoint(mouse.dragStartPosition);
        this.translatePoint(mouse.dragEndPosition);

        return mouse;
    };

    Tools.prototype.mouseUp = function (mouse) {
        this.currentTool.mouseUp(this.translateMouse(mouse));
    };
    Tools.prototype.mouseDown = function (mouse) {
        this.currentTool.mouseDown(this.translateMouse(mouse));
    };
    Tools.prototype.mouseMove = function (mouse) {
        this.currentTool.mouseMove(this.translateMouse(mouse));
    };

    Tools.prototype.toolInterface = function (name) {
        return $("#" + name + "-interface");
    };

    Tools.prototype.setTool = function (toolName) {
        var lastTool = this.currentTool;
        var elem = $("#whichtool");

        // I have no clue why this is required, but it is.
        elem.text(toolName);

        var toolInterface = this.toolInterface(toolName);
        var lastToolInterface = this.toolInterface(lastTool.toolName());

        // Order is important because you can switch to the same tool.
        lastToolInterface.hide();
        toolInterface.show();

        for (var i = 0; i < this.tools.length; i++) {
            if (this.tools[i].toolName() == toolName) {
                this.currentTool = this.tools[i];
            }
        }
    };
    return Tools;
})();

$(document).ready(function () {
    var c = new Canvas();

    $("#debugging").toggle();
});
